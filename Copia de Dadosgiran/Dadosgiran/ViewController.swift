//
//  ViewController.swift
//  Dadosgiran
//
//  Created by Mac7 on 10/11/19.
//  Copyright © 2019 itmorelia. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var dados: [UIImage] = []
    @IBOutlet var dado1: UIImageView!
    @IBOutlet var label: UILabel!
    @IBOutlet var Dado2: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    @IBAction func Roll(_ sender: Any) {
        dados.append(UIImage(named: "dice1")!)
         dados.append(UIImage(named: "dice2")!)
         dados.append(UIImage(named: "dice3")!)
         dados.append(UIImage(named: "dice4")!)
         dados.append(UIImage(named: "dice5")!)
         dados.append(UIImage(named: "dice6")!)
        dado1.image = dados.randomElement()
        Dado2.image =   dados.randomElement()
    }
    

}

